package ru.spbstu.dis.ita.environment;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import static org.mockito.BDDMockito.*;
import org.mockito.Matchers;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import ru.spbstu.dis.ita.agent.Agent;
import ru.spbstu.dis.ita.agent.NotCommunicatingAgent;
import ru.spbstu.dis.ita.agent.NotInteractiveAgent;
import ru.spbstu.dis.ita.agent.PerceivedData;
import ru.spbstu.dis.ita.agent.decision.AgentDecision;
import ru.spbstu.dis.ita.agent.goal.AgentGoal;
import ru.spbstu.dis.ita.agent.state.AgentState;

public class SimulatedTransportEnvironmentTest {
  @Test
  public void shouldStartWithZeroEpochAndIncreaseEpoch() {
    // given
    final SimulatedTransportEnvironment simulatedTransportEnvironment =
        new SimulatedTransportEnvironment(
            Lists.newArrayList(),
            Lists.newArrayList(),
            Maps.newHashMap(),
            Maps.newHashMap());

    // when
    simulatedTransportEnvironment.transitToNextEpoch();

    // then
    final int epoch = simulatedTransportEnvironment.getEpoch();
    assertThat(epoch).isEqualTo(1);
  }

  @Test
  public void shouldAcceptAndExecuteRegisteredDecisions() {
    // given
    final NothingDoingAgent agent1 = mock(NothingDoingAgent.class);
    final AgentDecision decision1 = mock(AgentDecision.class);
    given(agent1.solveAndDecide(isA(TransportEnvironment.class))).willReturn(decision1);
    willCallRealMethod().given(agent1).executeLifeCycle(Matchers.<TransportEnvironment>any());

    final NothingDoingAgent agent2 = mock(NothingDoingAgent.class);
    final AgentDecision decision2 = mock(AgentDecision.class);
    given(agent2.solveAndDecide(isA(TransportEnvironment.class))).willReturn(decision2);
    willCallRealMethod().given(agent2).executeLifeCycle(Matchers.<TransportEnvironment>any());

    final SimulatedTransportEnvironment simulatedTransportEnvironment =
        new SimulatedTransportEnvironment(
            Lists.newArrayList(agent1, agent2),
            Lists.newArrayList(),
            Maps.newHashMap(),
            Maps.newHashMap());

    // when
    simulatedTransportEnvironment.transitToNextEpoch();

    // then
    verify(decision1).implementDecision(simulatedTransportEnvironment);
    verify(decision2).implementDecision(simulatedTransportEnvironment);
  }
}

class NothingDoingAgent implements NotCommunicatingAgent, NotInteractiveAgent{
  @Override
  public AgentDecision solveAndDecide(final TransportEnvironment environment) {
    return null;
  }

  @Override
  public AgentState agentState() {
    return null;
  }

  @Override
  public void setNewAgentState(final AgentState newAgentState) {
  }

  @Override
  public AgentGoal agentGoal() {
    return null;
  }
}

class MovingNotInteracAgent implements NotCommunicatingAgent, NotInteractiveAgent{
    @Override
    public AgentDecision solveAndDecide(final TransportEnvironment environment) {
        return null;
    }

    @Override
    public AgentState agentState() {
        return null;
    }

    @Override
    public void setNewAgentState(final AgentState newAgentState) {
    }

    @Override
    public AgentGoal agentGoal() {
        return null;
    }
}