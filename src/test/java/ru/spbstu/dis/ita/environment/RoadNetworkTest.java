package ru.spbstu.dis.ita.environment;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;

public class RoadNetworkTest {

  @Test
  public void shouldAddRoadCrossings()
  throws Exception {
    // given
    final RoadNetwork roadNetwork = new RoadNetwork();

    // when
    roadNetwork.addRoadCrossing(new RoadCrossing(0, 1));
    roadNetwork.addRoadCrossing(new RoadCrossing(1, 0));
    roadNetwork.addRoadCrossing(new RoadCrossing(0, 0));
    roadNetwork.addRoadCrossing(new RoadCrossing(1, 1));

    // then
    assertThat(roadNetwork.graph.vertexSet().size()).isEqualTo(4);
  }

  @Test
  public void shouldNotAddSameCrossings() {
    // given
    final RoadNetwork roadNetwork = new RoadNetwork();

    // when
    roadNetwork.addRoadCrossing(new RoadCrossing(0, 1));
    roadNetwork.addRoadCrossing(new RoadCrossing(0, 1));
    roadNetwork.addRoadCrossing(new RoadCrossing(0, 1));
    roadNetwork.addRoadCrossing(new RoadCrossing(0, 1));

    // then
    assertThat(roadNetwork.graph.vertexSet().size()).isEqualTo(1);
    assertThat(roadNetwork.graph.vertexSet().iterator().next()).isEqualTo(new RoadCrossing(0, 1));
  }

  @Test
  public void shouldAddRoads() {
    // given
    final RoadNetwork roadNetwork = new RoadNetwork();
    roadNetwork.addRoadCrossing(new RoadCrossing(0, 1));
    roadNetwork.addRoadCrossing(new RoadCrossing(1, 0));
    roadNetwork.addRoadCrossing(new RoadCrossing(0, 0));
    roadNetwork.addRoadCrossing(new RoadCrossing(1, 1));

    // when
    roadNetwork.addRoad(new Road(new RoadCrossing(0, 1), new RoadCrossing(1, 0)));
    roadNetwork.addRoad(new Road(new RoadCrossing(1, 1), new RoadCrossing(1, 0)));

    // then
    assertThat(roadNetwork.graph.edgeSet().size()).isEqualTo(2);
  }

  @Test
  public void shouldNotAddSameRoads() {
    // given
    final RoadNetwork roadNetwork = new RoadNetwork();
    roadNetwork.addRoadCrossing(new RoadCrossing(0, 1));
    roadNetwork.addRoadCrossing(new RoadCrossing(1, 0));
    roadNetwork.addRoadCrossing(new RoadCrossing(0, 0));
    roadNetwork.addRoadCrossing(new RoadCrossing(1, 1));

    // when
    roadNetwork.addRoad(new Road(new RoadCrossing(0, 1), new RoadCrossing(1, 0)));
    roadNetwork.addRoad(new Road(new RoadCrossing(0, 1), new RoadCrossing(1, 0)));
    roadNetwork.addRoad(new Road(new RoadCrossing(0, 1), new RoadCrossing(1, 0)));
    roadNetwork.addRoad(new Road(new RoadCrossing(0, 1), new RoadCrossing(1, 0)));

    // then
    assertThat(roadNetwork.graph.edgeSet().size()).isEqualTo(1);
    assertThat(roadNetwork.graph.edgeSet().iterator().next())
        .isEqualTo(new Road(new RoadCrossing(0, 1), new RoadCrossing(1, 0)));
  }

  @Test
  public void shouldBeUndirected() {
    // given
    final RoadNetwork roadNetwork = new RoadNetwork();
    roadNetwork.addRoadCrossing(new RoadCrossing(0, 1));
    roadNetwork.addRoadCrossing(new RoadCrossing(1, 0));

    // when
    roadNetwork.addRoad(new Road(new RoadCrossing(0, 1), new RoadCrossing(1, 0)));
    roadNetwork.addRoad(new Road(new RoadCrossing(1, 0), new RoadCrossing(0, 1)));

    // then
    assertThat(roadNetwork.graph.edgeSet().size()).isEqualTo(1);
  }
}