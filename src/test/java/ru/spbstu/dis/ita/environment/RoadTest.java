package ru.spbstu.dis.ita.environment;

import static org.assertj.core.api.Assertions.assertThat;
import org.assertj.core.data.Offset;
import org.junit.Test;

public class RoadTest {

  @Test
  public void shouldComputeRoadLengthAppropriately() {
    // given
    final Road road = new Road(new RoadCrossing(0, 0), new RoadCrossing(1, 1));

    // when
    final double staticRoadWeight = road.roadLength();

    // then
    assertThat(staticRoadWeight).isCloseTo(Math.sqrt(2), Offset.offset(1e-5));
  }

  @Test
  public void twoRoadsShouldBeEqualDespiteDefinitionOrder() {
    //given
    final Road road = new Road(new RoadCrossing(0, 1), new RoadCrossing(1, 0));
    final Road road1 = new Road(new RoadCrossing(1, 0), new RoadCrossing(0, 1));

    // when
    final boolean equals = road.equals(road1);

    // then
    assertThat(equals).isTrue();
  }
}