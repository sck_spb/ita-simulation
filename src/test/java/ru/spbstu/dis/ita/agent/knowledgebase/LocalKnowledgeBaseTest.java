package ru.spbstu.dis.ita.agent.knowledgebase;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import ru.spbstu.dis.ita.agent.PerceivedData;
import java.util.Arrays;
import java.util.List;

public class LocalKnowledgeBaseTest {
  @Test
  public void shouldLocalKnowledgeBaseComputeRuleOutputBelowZero() {
    // given
    LocalKnowledgeBase localKnowledgeBase = new LocalKnowledgeBase();
    // when
    PerceivedData numberOfCarsData = new PerceivedData("numberOfCars", 0.77d, 0d, 1d);
    // accedent var
    PerceivedData carAheadBrokenData = new PerceivedData("carAheadBroken", 0.9d, 0d, 1d);
    // output var
    KnowledgeBaseMultiplierCoefficient coefficient = new KnowledgeBaseMultiplierCoefficient("jam");
    // rules
    List<KnowledgeBaseRule> rules = Arrays.asList(
        new KnowledgeBaseFuzyRule("if numberOfCars " + "is LOW then jam is LOW"),
        new KnowledgeBaseFuzyRule("if numberOfCars is MEDIUM then jam is MEDIUM"),
        new KnowledgeBaseFuzyRule("if numberOfCars is HIGH then jam is HIGH"),
        new KnowledgeBaseFuzyRule("if carAheadBroken is HIGH then jam is HIGH"));
    //then data
    localKnowledgeBase.setRuleData(Arrays.asList(numberOfCarsData, carAheadBrokenData),
        Arrays.asList(coefficient), rules);
    // then
    List<KnowledgeBaseMultiplierCoefficient> output = localKnowledgeBase
        .computeKBMultiplierCoefficients();
    Double singleOutput = output.get(0).getCoefficient();
    System.out.println("Output is " + singleOutput);
    assertThat(singleOutput).isGreaterThan(0.0);
  }
}