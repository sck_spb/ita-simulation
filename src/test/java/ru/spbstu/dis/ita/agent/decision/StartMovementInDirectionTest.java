package ru.spbstu.dis.ita.agent.decision;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.RETURNS_SMART_NULLS;
import ru.spbstu.dis.ita.agent.Agent;
import ru.spbstu.dis.ita.agent.WrongAgentStateException;
import ru.spbstu.dis.ita.agent.state.AgentState;
import ru.spbstu.dis.ita.agent.state.InDirectedMovement;
import ru.spbstu.dis.ita.environment.Road;
import ru.spbstu.dis.ita.environment.RoadCrossing;
import ru.spbstu.dis.ita.environment.TransportEnvironment;

public class StartMovementInDirectionTest {
  @Test
  public void shouldStartMovingFromCrossing() {
    // given
    final RoadCrossing from = RoadCrossing.withCoordinates(0, 0);
    final RoadCrossing to = RoadCrossing.withCoordinates(0, 1);
    final Road road = Road.betweenCrossings(from, to);

    final TransportEnvironment env = mock(TransportEnvironment.class);
    final Agent agent = mock(Agent.class, RETURNS_SMART_NULLS);
    given(agent.agentState()).willReturn(AgentState.onRoadCrossing(from));

    final StartMovementInDirection startMovementInDirection =
        new StartMovementInDirection(to, agent);
    final ArgumentCaptor<AgentState> stateCaptor = ArgumentCaptor.forClass(AgentState.class);

    // when
    startMovementInDirection.implementDecision(env);

    // then
    then(agent).should().setNewAgentState(stateCaptor.capture());
    final AgentState agentState = stateCaptor.getValue();
    assertThat(agentState).isInstanceOf(InDirectedMovement.class);
    assertThat(((InDirectedMovement) agentState).distanceFromStart()).isEqualTo(0);
    assertThat(((InDirectedMovement) agentState).road()).isEqualTo(road);
    assertThat(((InDirectedMovement) agentState).movingTowards()).isEqualTo(to);
  }

  @Test(expected = WrongAgentStateException.class)
  public void shouldExceptWhenStartingMovementInMovement() {
    // given
    final RoadCrossing from = RoadCrossing.withCoordinates(0, 0);
    final RoadCrossing to = RoadCrossing.withCoordinates(0, 1);
    final Road road = Road.betweenCrossings(from, to);

    final TransportEnvironment env = mock(TransportEnvironment.class);
    final Agent agent = mock(Agent.class, RETURNS_SMART_NULLS);
    given(agent.agentState()).willReturn(AgentState.inMovement(1, to, road, 0));

    final StartMovementInDirection startMovementInDirection =
        new StartMovementInDirection(to, agent);

    // when
    startMovementInDirection.implementDecision(env);
  }
}