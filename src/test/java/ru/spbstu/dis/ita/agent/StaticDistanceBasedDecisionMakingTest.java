package ru.spbstu.dis.ita.agent;

import com.google.common.collect.Lists;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import ru.spbstu.dis.ita.agent.decision.AgentDecision;
import ru.spbstu.dis.ita.agent.decision.ContinueMovementInCurrentDirection;
import ru.spbstu.dis.ita.agent.decision.StartMovementInDirection;
import ru.spbstu.dis.ita.agent.decision.StayOnRoadCrossing;
import ru.spbstu.dis.ita.agent.state.AgentState;
import ru.spbstu.dis.ita.environment.Road;
import ru.spbstu.dis.ita.environment.RoadCrossing;
import ru.spbstu.dis.ita.environment.RoadNetwork;

public class StaticDistanceBasedDecisionMakingTest {
  private RoadNetwork roadNetwork() {
    final RoadCrossing c00 = RoadCrossing.withCoordinates(0, 0);
    final RoadCrossing c22 = RoadCrossing.withCoordinates(2, 2);
    final RoadCrossing c02 = RoadCrossing.withCoordinates(0, 2);
    final RoadCrossing c21 = RoadCrossing.withCoordinates(2, 1);
    final RoadNetwork roadNetwork = new RoadNetwork();
    roadNetwork.addRoadWithCrossings(Road.betweenCrossings(c00, c02));
    roadNetwork.addRoadWithCrossings(Road.betweenCrossings(c00, c21));
    roadNetwork.addRoadWithCrossings(Road.betweenCrossings(c02, c22));
    roadNetwork.addRoadWithCrossings(Road.betweenCrossings(c21, c22));
    return roadNetwork;
  }

  @Test
  public void shouldPerformDistanceBasedDecisionMaking_OnCrossing() {
    // given
    final StaticDistanceBasedDecisionMaking staticDistanceBasedDecisionMaking =
        new StaticDistanceBasedDecisionMaking(
            RoadCrossing.withCoordinates(0, 0),
            RoadCrossing.withCoordinates(2, 2),
            roadNetwork());
    final Agent agent = mock(Agent.class, RETURNS_SMART_NULLS);
    given(agent.agentState()).willReturn(
        AgentState.onRoadCrossing(RoadCrossing.withCoordinates(0, 0)));

    // when
    final AgentDecision decision = staticDistanceBasedDecisionMaking.decide(agent);

    // then
    assertThat(decision).isInstanceOf(StartMovementInDirection.class);
    assertThat(((StartMovementInDirection) decision).towards())
        .isEqualTo(RoadCrossing.withCoordinates(2, 1));
  }

  @Test
  public void shouldPerformDistanceBasedDecisionMaking_InMovement() {
    // given
    final StaticDistanceBasedDecisionMaking staticDistanceBasedDecisionMaking =
        new StaticDistanceBasedDecisionMaking(
            RoadCrossing.withCoordinates(0, 0),
            RoadCrossing.withCoordinates(2, 2),
            roadNetwork());
    final Agent agent = mock(Agent.class, RETURNS_SMART_NULLS);
    given(agent.agentState()).willReturn(
        AgentState.inMovement(.0,
            RoadCrossing.withCoordinates(2, 2),
            Road.betweenCrossings(
                RoadCrossing.withCoordinates(2, 1),
                RoadCrossing.withCoordinates(2, 2)),
            .4));

    // when
    final AgentDecision decision = staticDistanceBasedDecisionMaking.decide(agent);

    // then
    assertThat(decision).isInstanceOf(ContinueMovementInCurrentDirection.class);
  }

  @Test
  public void shouldPerformDistanceBasedDecisionMaking_InGoalState() {
    // given
    final StaticDistanceBasedDecisionMaking staticDistanceBasedDecisionMaking =
        new StaticDistanceBasedDecisionMaking(
            RoadCrossing.withCoordinates(0, 0),
            RoadCrossing.withCoordinates(2, 2),
            roadNetwork());
    final Agent agent = mock(Agent.class, RETURNS_SMART_NULLS);
    given(agent.agentState()).willReturn(
        AgentState.onRoadCrossing(RoadCrossing.withCoordinates(2, 2)));

    // when
    final AgentDecision decision = staticDistanceBasedDecisionMaking.decide(agent);

    // then
    assertThat(decision).isInstanceOf(StayOnRoadCrossing.class);
    assertThat(((StayOnRoadCrossing) decision).roadCrossing())
        .isEqualTo(RoadCrossing.withCoordinates(2, 2));
  }
}