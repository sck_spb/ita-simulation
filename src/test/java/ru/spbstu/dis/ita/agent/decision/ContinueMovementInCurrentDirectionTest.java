package ru.spbstu.dis.ita.agent.decision;

import static org.assertj.core.api.Assertions.*;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import static org.mockito.BDDMockito.*;
import ru.spbstu.dis.ita.agent.Agent;
import ru.spbstu.dis.ita.agent.WrongAgentStateException;
import ru.spbstu.dis.ita.agent.state.AgentState;
import ru.spbstu.dis.ita.agent.state.InDirectedMovement;
import ru.spbstu.dis.ita.agent.state.OnRoadCrossing;
import ru.spbstu.dis.ita.environment.Road;
import ru.spbstu.dis.ita.environment.RoadCrossing;

public class ContinueMovementInCurrentDirectionTest {

  @Test
  public void shouldContinueMovementInRightDirectionWhenInMovement() {
    // given
    final Agent agent = mock(Agent.class);
    final InDirectedMovement movement = mock(InDirectedMovement.class);
    given(movement.speed()).willReturn(1.0);
    given(movement.distanceFromStart()).willReturn(.0);
    given(movement.movingTowards()).willReturn(new RoadCrossing(1, 5));
    given(movement.road()).willReturn(new Road(new RoadCrossing(0, 0), new RoadCrossing(1, 5)));
    given(agent.agentState()).willReturn(movement);
    final ContinueMovementInCurrentDirection continueMovementInCurrentDirection
        = new ContinueMovementInCurrentDirection(agent);
    final ArgumentCaptor<AgentState> stateCaptor = ArgumentCaptor.forClass(AgentState.class);

    // when
    continueMovementInCurrentDirection.implementDecision(null);

    // then
    then(agent).should().setNewAgentState(stateCaptor.capture());
    final AgentState agentState = stateCaptor.getValue();
    assertThat(agentState).isInstanceOf(InDirectedMovement.class);
    assertThat(((InDirectedMovement) agentState).distanceFromStart())
        .isCloseTo(.0 + movement.speed(), offset(1e-5));
  }

  @Test
  public void shouldEndInRoadCrossingWhenInMovement()
  throws Exception {
    // given
    final Agent agent = mock(Agent.class);
    final InDirectedMovement movement = mock(InDirectedMovement.class);
    given(movement.speed()).willReturn(Math.sqrt(2) + 0.123);
    given(movement.distanceFromStart()).willReturn(.0);
    given(movement.movingTowards()).willReturn(new RoadCrossing(1, 1));
    given(movement.road()).willReturn(new Road(new RoadCrossing(0, 0), new RoadCrossing(1, 1)));
    given(agent.agentState()).willReturn(movement);
    final ContinueMovementInCurrentDirection continueMovementInCurrentDirection
        = new ContinueMovementInCurrentDirection(agent);
    final ArgumentCaptor<AgentState> stateCaptor = ArgumentCaptor.forClass(AgentState.class);

    // when
    continueMovementInCurrentDirection.implementDecision(null);

    // then
    then(agent).should().setNewAgentState(stateCaptor.capture());
    final AgentState agentState = stateCaptor.getValue();
    assertThat(agentState).isInstanceOf(OnRoadCrossing.class);
  }

  @Test(expected = WrongAgentStateException.class)
  public void shouldThrowIfNotInMovement() {
    // given
    final Agent agent = mock(Agent.class);
    final OnRoadCrossing onRoadCrossing = mock(OnRoadCrossing.class);
    given(agent.agentState()).willReturn(onRoadCrossing);
    final ContinueMovementInCurrentDirection continueMovementInCurrentDirection
        = new ContinueMovementInCurrentDirection(agent);
    final ArgumentCaptor<AgentState> stateCaptor = ArgumentCaptor.forClass(AgentState.class);

    // when
    continueMovementInCurrentDirection.implementDecision(null);

    // then
  }
}