package ru.spbstu.dis.ita.simulation;

import com.google.common.collect.Lists;
import ru.spbstu.dis.ita.agent.StaticDistanceBasedDecisionMakingAgent;
import ru.spbstu.dis.ita.environment.RoadNetwork;
import ru.spbstu.dis.ita.environment.SimulatedTransportEnvironment;
import java.util.stream.IntStream;

/**
 * Generates environment, executs and collects statics
 */
public class Simulation {
  private int numberOfAgents;

  public Simulation(int numberOfAgents) {
    this.numberOfAgents = numberOfAgents;
  }

  public void initializeSimulation() {
    IntStream.range(0, numberOfAgents).boxed().map(integer ->
      new StaticDistanceBasedDecisionMakingAgent(null, null));

    new SimulatedTransportEnvironment(null, null, null, null);
  }

  private RoadNetwork simulationRoadNetwork() {

    return null;
  }
}
