package ru.spbstu.dis.ita.environment;

import org.jgrapht.graph.DefaultWeightedEdge;

public class Road extends DefaultWeightedEdge {
  private final RoadCrossing from;

  private final RoadCrossing to;

  public Road(RoadCrossing from, RoadCrossing to) {
    this.from = from;
    this.to = to;
  }

  public RoadCrossing getFrom() {
    return from;
  }

  public RoadCrossing getTo() {
    return to;
  }

  public RoadCrossing oppositeEndOf(RoadCrossing end) {
    if (from == end) {
      return to;
    } else {
      return from;
    }
  }

  public double roadLength() {
    return getWeight();
  }

  @Override
  protected double getWeight() {
    // not cached
    return
        Math.sqrt(.0
            + (from.x() - to.x()) * (from.x() - to.x())
            + (from.y() - to.y()) * (from.y() - to.y()))*super.getWeight();
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    final Road road = (Road) o;

    if (from.equals(road.from) && to.equals(road.to)
        || from.equals(road.to) && to.equals(road.from)) {
      return true;
    }
    return false;
  }

  @Override
  public int hashCode() {
    int result = from.hashCode();
    result = 31 * result + to.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "Road{" +
        "from=" + from +
        ", to=" + to +
        " with wieght="+getWeight()+'}';
  }

  public static Road betweenCrossings(RoadCrossing from, RoadCrossing to) {
    return new Road(from, to);
  }
}