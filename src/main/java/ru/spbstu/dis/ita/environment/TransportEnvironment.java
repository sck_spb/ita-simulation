package ru.spbstu.dis.ita.environment;

import ru.spbstu.dis.ita.agent.Agent;
import ru.spbstu.dis.ita.messaging.Message;
import ru.spbstu.dis.ita.agent.decision.AgentDecision;
import java.util.List;

public interface TransportEnvironment {
  /**
   * Shift environment clock to next tick and execute environment logic: dispatch messages,
   * implement decisions, etc
   */
  void transitToNextEpoch();

  int getEpoch();

  /**
   * Called from agent lifecycle to retrieve all messages that were directed to given agent in
   * the previous epoch
   * @param agent
   * @return
   */
  List<Message> incomingMessagesFor(Agent agent);

  /**
   * Called from agent lifecycle to register outgoing messages that are going to be dispatched in
   * the next epoch
   * @param agent
   * @param messages
   */
  void registerOutgoingMessages(final Agent agent, List<Message> messages);

  /**
   * Called from agent lifecycle to register a decision that will be implemented by environment
   * in the next epoch
   * @param agentDecision
   */
  void registerDecision(AgentDecision agentDecision);

  // shared knowledge

  RoadNetwork roadNetwork();
}