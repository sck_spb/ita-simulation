package ru.spbstu.dis.ita.environment;

import com.google.common.collect.Lists;
import ru.spbstu.dis.ita.agent.Agent;
import ru.spbstu.dis.ita.agent.decision.AgentDecision;
import ru.spbstu.dis.ita.messaging.Message;
import java.util.List;
import java.util.Map;

public class SimulatedTransportEnvironment implements TransportEnvironment {
  private int epoch;

  private List<Agent> agentList;

  private List<AgentDecision> currentEpochDecisions;

  private Map<Agent, List<Message>> currentEpochIncomingMessages;

  private Map<Agent, List<Message>> currentEpochOutgoingMessages;

  public SimulatedTransportEnvironment(
      final List<Agent> initialAgentList,
      final List<AgentDecision> initialDecisions,
      final Map<Agent, List<Message>> initialIncomingMessages,
      final Map<Agent, List<Message>> initialOutgoingMessages) {
    this.agentList = initialAgentList;
    this.currentEpochDecisions = initialDecisions;
    this.currentEpochIncomingMessages = initialIncomingMessages;
    this.currentEpochOutgoingMessages = initialOutgoingMessages;
  }

  @Override
  public void transitToNextEpoch() {
    epoch += 1;
    agentList.forEach(itaAgent -> itaAgent.executeLifeCycle(this));
    // todo dispatch messages
    executeDecisions();
    // todo represent world state
  }

  private void executeDecisions() {
    currentEpochDecisions.forEach(agentDecision -> agentDecision.implementDecision(this));
    currentEpochDecisions = Lists.newLinkedList();
  }

  @Override
  public List<Message> incomingMessagesFor(final Agent agent) {
    return currentEpochIncomingMessages.getOrDefault(agent, Lists.newArrayList());
  }

  @Override
  public void registerOutgoingMessages(final Agent agent,
      final List<Message> messages) {
    currentEpochOutgoingMessages.compute(agent, (a, m) -> {
      if (m == null) {
        return messages;
      } else {
        m.addAll(messages);
        return m;
      }
    });
  }

  @Override
  public void registerDecision(final AgentDecision agentDecision) {
    currentEpochDecisions.add(agentDecision);
  }

  @Override
  public RoadNetwork roadNetwork() {
    return null;
  }

  @Override
  public int getEpoch() {
    return epoch;
  }

  List<Agent> getAgentList() {
    return agentList;
  }
}