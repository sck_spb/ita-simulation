package ru.spbstu.dis.ita.environment;

public class RoadCrossing {
  private final double x;

  private final double y;

  public RoadCrossing(double x, double y) {
    this.x = x;
    this.y = y;
  }

  public double x() {
    return x;
  }

  public double y() {
    return y;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    final RoadCrossing that = (RoadCrossing) o;

    if (x != that.x) {
      return false;
    }
    return y == that.y;
  }

  @Override
  public int hashCode() {
    int result;
    long temp;
    temp = Double.doubleToLongBits(x);
    result = (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(y);
    result = 31 * result + (int) (temp ^ (temp >>> 32));
    return result;
  }

  @Override
  public String toString() {
    return "RoadCrossing{" +
        "x=" + x +
        ", y=" + y +
        '}';
  }

  public static RoadCrossing withCoordinates(double x, double y) {
    return new RoadCrossing(x, y);
  }
}
