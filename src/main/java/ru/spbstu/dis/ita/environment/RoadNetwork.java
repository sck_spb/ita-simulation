package ru.spbstu.dis.ita.environment;

import org.jgrapht.graph.SimpleWeightedGraph;

/**
 * Represents a road network used in environment and inside of ITA
 */
public class RoadNetwork {
  /**
   * Uses weighted graph
   */
  final SimpleWeightedGraph<RoadCrossing, Road> graph;

  public RoadNetwork() {
    graph = new SimpleWeightedGraph<>(Road::new);
  }

  public void addRoadCrossing(RoadCrossing roadCrossing) {
    graph.addVertex(roadCrossing);
  }

  public void addRoad(Road road) {
    graph.addEdge(road.getFrom(), road.getTo());
  }

  public void addRoadWithCrossings(Road road) {
    graph.addVertex(road.getFrom());
    graph.addVertex(road.getTo());
    graph.addEdge(road.getFrom(), road.getTo());
  }

  public SimpleWeightedGraph<RoadCrossing, Road> graph() {
    return graph;
  }

  @Override
  public String toString() {
    return "RoadNetwork{" +
        "graph=" + graph +
        '}';
  }
}