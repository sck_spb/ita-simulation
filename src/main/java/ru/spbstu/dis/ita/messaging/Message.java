package ru.spbstu.dis.ita.messaging;

import ru.spbstu.dis.ita.agent.Agent;

public interface Message {
  Agent from();

  Agent to();
}

