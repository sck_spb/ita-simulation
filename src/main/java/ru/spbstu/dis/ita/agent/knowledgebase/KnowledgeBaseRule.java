package ru.spbstu.dis.ita.agent.knowledgebase;

public interface KnowledgeBaseRule {
	public String getRuleAsString();
	public Object getRuleObject();
}
