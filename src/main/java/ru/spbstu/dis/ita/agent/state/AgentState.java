package ru.spbstu.dis.ita.agent.state;

import ru.spbstu.dis.ita.environment.Road;
import ru.spbstu.dis.ita.environment.RoadCrossing;

public interface AgentState {
  public static double DEFAULT_SPEED = 2.0;

  static InDirectedMovement inMovementDefaultSpeed(
      RoadCrossing movingTowards,
      Road road,
      double distanceFromStart) {
    return inMovement(DEFAULT_SPEED, movingTowards, road, distanceFromStart);
  }

  static InDirectedMovement inMovement(
      double speed,
      RoadCrossing movingTowards,
      Road road,
      double distanceFromStart) {
    return new DefaultInDirectedMovement(speed, movingTowards, road, distanceFromStart);
  }

  static OnRoadCrossing onRoadCrossing(RoadCrossing crossing) {
    return new DefaultOnRoadCrossing(crossing);
  }
}

