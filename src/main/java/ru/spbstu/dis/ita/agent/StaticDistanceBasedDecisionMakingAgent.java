package ru.spbstu.dis.ita.agent;

import org.jgrapht.GraphPath;
import org.jgrapht.alg.DijkstraShortestPath;
import ru.spbstu.dis.ita.agent.decision.AgentDecision;
import ru.spbstu.dis.ita.agent.goal.AgentGoal;
import ru.spbstu.dis.ita.agent.goal.RoadCrossingGoal;
import ru.spbstu.dis.ita.agent.state.AgentState;
import ru.spbstu.dis.ita.agent.state.OnRoadCrossing;
import ru.spbstu.dis.ita.environment.Road;
import ru.spbstu.dis.ita.environment.RoadCrossing;
import ru.spbstu.dis.ita.environment.RoadNetwork;
import ru.spbstu.dis.ita.environment.TransportEnvironment;

public class StaticDistanceBasedDecisionMakingAgent
    implements NotCommunicatingAgent, NotInteractiveAgent {

  private final RoadCrossingGoal agentGoal;

  private AgentState currentState;

  private StaticDistanceBasedDecisionMaking staticDistanceBasedDecisionMaking;

  public StaticDistanceBasedDecisionMakingAgent(OnRoadCrossing initialState, RoadCrossingGoal agentGoal) {
    this.currentState = initialState;
    this.agentGoal = agentGoal;
  }

  @Override
  public AgentDecision solveAndDecide(final TransportEnvironment environment) {
    if (notInitialized()) {
      if (currentState instanceof OnRoadCrossing) {
        final RoadCrossing currentCrossing = ((OnRoadCrossing) currentState).crossing();
        staticDistanceBasedDecisionMaking =
            new StaticDistanceBasedDecisionMaking(
                currentCrossing,
                agentGoal.crossing(),
                environment.roadNetwork());
        return staticDistanceBasedDecisionMaking.decide(this);
      } else {
        throw new WrongAgentStateException(
            String.format(
                "Could not initialize StaticDistanceBasedDecisionMakingAgent in %s", currentState));
      }
    } else {
      return staticDistanceBasedDecisionMaking.decide(this);
    }
  }

  private boolean notInitialized() {
    return staticDistanceBasedDecisionMaking == null;
  }

  @Override
  public AgentState agentState() {
    return currentState;
  }

  @Override
  public void setNewAgentState(final AgentState newAgentState) {
    currentState = newAgentState;
  }

  @Override
  public AgentGoal agentGoal() {
    return agentGoal;
  }
}

class StaticDistanceBasedDecisionMaking {
  private final RoadCrossing goalCrossing;

  private final RoadNetwork roadNetwork;

  private final RoadCrossing initialCrossing;

  private GraphPath<RoadCrossing, Road> movementPath;

  StaticDistanceBasedDecisionMaking(
      RoadCrossing nearestCrossingAtTheDecisionMoment,
      RoadCrossing goalCrossing,
      RoadNetwork roadNetwork) {
    initialCrossing = nearestCrossingAtTheDecisionMoment;
    this.goalCrossing = goalCrossing;
    this.roadNetwork = roadNetwork;
  }

  public GraphPath<RoadCrossing, Road> movementPath() {
    return movementPath;
  }

  public AgentDecision decide(Agent agent) {
    if (movementPath == null) {
      computePath();
    }
    final AgentState state = agent.agentState();
    if (reachedGoalState(state)) {
      return AgentDecision.stayOnRoadCrossing(goalCrossing, agent);
    }
    if (state instanceof OnRoadCrossing) {
      final OnRoadCrossing onCrossing = (OnRoadCrossing) state;
      final Road roadWeMoveOn =
          movementPath.getEdgeList().stream()
              .filter(road -> road.getFrom().equals(onCrossing.crossing())).findFirst().get();
      return AgentDecision.startMovementInDirection(roadWeMoveOn.getTo(), agent);
    } else {
      return AgentDecision.continueMovementInCurrentDirection(agent);
    }
  }

  private void computePath() {
    final DijkstraShortestPath<RoadCrossing, Road> dijkstraShortestPath
        = new DijkstraShortestPath<>(
        roadNetwork.graph(),
        initialCrossing,
        goalCrossing);
    this.movementPath = dijkstraShortestPath.getPath();
  }

  private boolean reachedGoalState(final AgentState state) {
    return state instanceof OnRoadCrossing
        && ((OnRoadCrossing) state).crossing().equals(goalCrossing);
  }
}