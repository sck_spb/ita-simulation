package ru.spbstu.dis.ita.agent.decision;

import ru.spbstu.dis.ita.agent.Agent;
import ru.spbstu.dis.ita.agent.WrongAgentStateException;
import ru.spbstu.dis.ita.agent.state.AgentState;
import ru.spbstu.dis.ita.agent.state.InDirectedMovement;
import ru.spbstu.dis.ita.agent.state.OnRoadCrossing;
import ru.spbstu.dis.ita.environment.Road;
import ru.spbstu.dis.ita.environment.RoadCrossing;
import ru.spbstu.dis.ita.environment.TransportEnvironment;

public class StartMovementInDirection implements AgentDecision {
  private RoadCrossing towards;

  private Agent agent;

  StartMovementInDirection(RoadCrossing towards, Agent agent) {
    this.towards = towards;
    this.agent = agent;
  }

  public RoadCrossing towards() {
    return towards;
  }

  @Override
  public Agent decisionOriginator() {
    return agent;
  }

  @Override
  public void implementDecision(final TransportEnvironment environment) {
    if (agent.agentState() instanceof OnRoadCrossing) {
      final OnRoadCrossing onCrossing = (OnRoadCrossing) agent.agentState();
      agent.setNewAgentState(startMoving(onCrossing));
    } else {
      throw new WrongAgentStateException("Wrong state to start movement");
    }
  }

  private InDirectedMovement startMoving(final OnRoadCrossing onCrossing) {
    return AgentState.inMovement(
        AgentState.DEFAULT_SPEED,
        towards,
        Road.betweenCrossings(onCrossing.crossing(), towards),
        .0);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    final StartMovementInDirection that = (StartMovementInDirection) o;

    if (!towards.equals(that.towards)) {
      return false;
    }
    return agent.equals(that.agent);
  }

  @Override
  public int hashCode() {
    int result = towards.hashCode();
    result = 31 * result + agent.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "StartMovementInDirection{" +
        "towards=" + towards +
        ", agent=" + agent +
        '}';
  }
}