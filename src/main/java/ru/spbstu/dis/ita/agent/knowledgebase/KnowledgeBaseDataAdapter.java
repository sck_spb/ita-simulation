package ru.spbstu.dis.ita.agent.knowledgebase;

import com.fuzzylite.term.Triangle;
import com.fuzzylite.variable.InputVariable;
import com.fuzzylite.variable.OutputVariable;
import ru.spbstu.dis.ita.agent.PerceivedData;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class KnowledgeBaseDataAdapter {
	public static Map<InputVariable, List<String>> generateFuzzyInput(List<PerceivedData> percievedDatas, List<String> rules){
		Map<InputVariable, List<String>> inputWithRules = new HashMap<>();
		percievedDatas.forEach(perceivedData -> {
			InputVariable inputVariable= new InputVariable(perceivedData.getName(),perceivedData.getMin(),perceivedData.getMax());
			inputVariable.setInputValue(perceivedData.getInputValue());
			//standart function for all rules
			inputVariable.addTerm(new Triangle("LOW", 0.000, 0.250, 0.500));
			inputVariable.addTerm(new Triangle("MEDIUM", 0.250, 0.500, 0.750));
			inputVariable.addTerm(new Triangle("HIGH", 0.500, 0.750, 1.000));
			inputWithRules.put(inputVariable,
					rules.stream().filter(s -> s.contains(perceivedData.getName())).collect(Collectors.toList()));
		});
		return inputWithRules;
	}

	public static List<OutputVariable> generateFuzzyOutput(List<KnowledgeBaseMultiplierCoefficient> coefficients){
		List<OutputVariable> outputVariables = new LinkedList<>();
		coefficients.forEach(kbCoefficient -> {
			OutputVariable outputVariable = new OutputVariable(kbCoefficient.getName());
			outputVariable.setDefaultValue(Double.NaN);
			outputVariable.setRange(0.000, 1.000);
			outputVariable.addTerm(new Triangle("LOW", 0.000, 0.250, 0.500));
			outputVariable.addTerm(new Triangle("MEDIUM", 0.250, 0.500, 0.750));
			outputVariable.addTerm(new Triangle("HIGH", 0.500, 0.750, 1.000));
			outputVariables.add(outputVariable);
		});
		return outputVariables;
	}
}
