package ru.spbstu.dis.ita.agent.decision;

import ru.spbstu.dis.ita.agent.Agent;
import ru.spbstu.dis.ita.agent.state.AgentState;
import ru.spbstu.dis.ita.environment.RoadCrossing;
import ru.spbstu.dis.ita.environment.TransportEnvironment;

public class StayOnRoadCrossing implements AgentDecision {

  private final RoadCrossing roadCrossing;

  private final Agent agent;

  public StayOnRoadCrossing(RoadCrossing roadCrossing, Agent agent) {
    this.roadCrossing = roadCrossing;
    this.agent = agent;
  }

  public RoadCrossing roadCrossing() {
    return roadCrossing;
  }

  @Override
  public Agent decisionOriginator() {
    return agent;
  }

  @Override
  public void implementDecision(final TransportEnvironment environment) {
    agent.setNewAgentState(AgentState.onRoadCrossing(roadCrossing));
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    final StayOnRoadCrossing that = (StayOnRoadCrossing) o;

    if (!agent.equals(that.agent)) {
      return false;
    }
    return roadCrossing.equals(that.roadCrossing);
  }

  @Override
  public int hashCode() {
    int result = agent.hashCode();
    result = 31 * result + roadCrossing.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "StayOnRoadCrossing{" +
        "agent=" + agent +
        ", roadCrossing=" + roadCrossing +
        '}';
  }
}
