package ru.spbstu.dis.ita.agent.state;

import ru.spbstu.dis.ita.environment.Road;
import ru.spbstu.dis.ita.environment.RoadCrossing;

class DefaultInDirectedMovement implements InDirectedMovement {
  private final double speed;

  private final RoadCrossing movingTowards;

  private final Road roadWeAreOn;

  private final double distanceFromStartOfRoad;

  DefaultInDirectedMovement(
      double speed,
      RoadCrossing movingTowards,
      Road roadWeAreOn,
      double distanceFromStartOfRoad) {
    this.speed = speed;
    this.movingTowards = movingTowards;
    this.roadWeAreOn = roadWeAreOn;
    this.distanceFromStartOfRoad = distanceFromStartOfRoad;
  }

  @Override
  public double speed() {
    return speed;
  }

  @Override
  public double distanceFromStart() {
    return distanceFromStartOfRoad;
  }

  @Override
  public Road road() {
    return roadWeAreOn;
  }

  @Override
  public RoadCrossing movingTowards() {
    return movingTowards;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    final DefaultInDirectedMovement that = (DefaultInDirectedMovement) o;

    if (Double.compare(that.speed, speed) != 0) {
      return false;
    }
    if (Double.compare(that.distanceFromStartOfRoad, distanceFromStartOfRoad) != 0) {
      return false;
    }
    if (movingTowards != null ? !movingTowards.equals(that.movingTowards) :
        that.movingTowards != null) {
      return false;
    }
    return !(
        roadWeAreOn != null ? !roadWeAreOn.equals(that.roadWeAreOn) : that.roadWeAreOn != null);
  }

  @Override
  public int hashCode() {
    int result;
    long temp;
    temp = Double.doubleToLongBits(speed);
    result = (int) (temp ^ (temp >>> 32));
    result = 31 * result + (movingTowards != null ? movingTowards.hashCode() : 0);
    result = 31 * result + (roadWeAreOn != null ? roadWeAreOn.hashCode() : 0);
    temp = Double.doubleToLongBits(distanceFromStartOfRoad);
    result = 31 * result + (int) (temp ^ (temp >>> 32));
    return result;
  }

  @Override
  public String toString() {
    return "DefaultInDirectedMovement{" +
        "speed=" + speed +
        ", movingTowards=" + movingTowards +
        ", roadWeAreOn=" + roadWeAreOn +
        ", distanceFromStartOfRoad=" + distanceFromStartOfRoad +
        '}';
  }
}