package ru.spbstu.dis.ita.agent;

public class PerceivedData {
	private String name;
	private Double inputValue;
	private Double min;
	private Double max;

	public PerceivedData(String name, Double inputValue, Double min, Double max) {
		this.name = name;
		this.inputValue = inputValue;
		this.min = min;
		this.max = max;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getInputValue() {
		return inputValue;
	}

	public void setInputValue(Double inputValue) {
		this.inputValue = inputValue;
	}

	public Double getMin() {
		return min;
	}

	public void setMin(Double min) {
		this.min = min;
	}

	public Double getMax() {
		return max;
	}

	public void setMax(Double max) {
		this.max = max;
	}
}
