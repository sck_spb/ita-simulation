package ru.spbstu.dis.ita.agent;

public class WrongAgentStateException extends RuntimeException {
  public WrongAgentStateException(final String message) {
    super(message);
  }
}
