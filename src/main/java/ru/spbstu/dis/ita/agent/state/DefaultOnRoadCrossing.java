package ru.spbstu.dis.ita.agent.state;

import ru.spbstu.dis.ita.environment.RoadCrossing;

class DefaultOnRoadCrossing implements OnRoadCrossing {
  private RoadCrossing crossing;

  DefaultOnRoadCrossing(RoadCrossing crossing) {
    this.crossing = crossing;
  }

  @Override
  public RoadCrossing crossing() {
    return crossing;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    final DefaultOnRoadCrossing that = (DefaultOnRoadCrossing) o;

    return crossing.equals(that.crossing);
  }

  @Override
  public int hashCode() {
    return crossing.hashCode();
  }

  @Override
  public String toString() {
    return "DefaultOnRoadCrossing{" +
        "crossing=" + crossing +
        '}';
  }
}
