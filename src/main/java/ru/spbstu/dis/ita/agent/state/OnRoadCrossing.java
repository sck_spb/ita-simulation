package ru.spbstu.dis.ita.agent.state;

import ru.spbstu.dis.ita.environment.RoadCrossing;

public interface OnRoadCrossing extends AgentState {
  RoadCrossing crossing();
}
