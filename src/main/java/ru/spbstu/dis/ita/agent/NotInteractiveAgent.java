package ru.spbstu.dis.ita.agent;

import ru.spbstu.dis.ita.environment.TransportEnvironment;

public interface NotInteractiveAgent extends Agent{
  @Override
  default PerceivedData perceiveData(TransportEnvironment environment) {
    return null;
  }

  @Override
  default void handlePerceivedData(PerceivedData newData) {
    // no-op
  }
}
