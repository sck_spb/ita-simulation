package ru.spbstu.dis.ita.agent;

import com.google.common.collect.Lists;
import ru.spbstu.dis.ita.messaging.Message;
import java.util.List;

public interface NotCommunicatingAgent extends Agent {

  @Override
  default void handleIncomingMessages(final List<Message> incomingMessagesBuffer) {

  }

  @Override
  default List<Message> generateOutgoingMessages() {
    return Lists.newArrayList();
  }
}