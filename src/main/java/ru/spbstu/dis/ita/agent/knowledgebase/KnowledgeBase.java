package ru.spbstu.dis.ita.agent.knowledgebase;

import ru.spbstu.dis.ita.agent.PerceivedData;

import java.util.List;

public interface KnowledgeBase {
  public List<KnowledgeBaseMultiplierCoefficient> computeKBMultiplierCoefficients();

  public void setRuleData(List<PerceivedData> inputValues, List<KnowledgeBaseMultiplierCoefficient> multiplierCoefficients,List<KnowledgeBaseRule> knowledgeBaseRules);
}

