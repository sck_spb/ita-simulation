package ru.spbstu.dis.ita.agent.decision;

import ru.spbstu.dis.ita.agent.Agent;
import ru.spbstu.dis.ita.agent.WrongAgentStateException;
import ru.spbstu.dis.ita.agent.state.AgentState;
import ru.spbstu.dis.ita.agent.state.InDirectedMovement;
import ru.spbstu.dis.ita.agent.state.OnRoadCrossing;
import ru.spbstu.dis.ita.environment.TransportEnvironment;

public class ContinueMovementInCurrentDirection implements AgentDecision {
  private Agent agent;

  public ContinueMovementInCurrentDirection(Agent agent) {
    this.agent = agent;
  }

  @Override
  public Agent decisionOriginator() {
    return agent;
  }

  @Override
  public void implementDecision(TransportEnvironment environment) {
    if (agent.agentState() instanceof InDirectedMovement) {
      final InDirectedMovement s = (InDirectedMovement) agent.agentState();
      if (haveSpaceToMove(s)) {
        agent.setNewAgentState(moveAgent(s));
      } else {
        agent.setNewAgentState(finishMovement(s));
      }
    } else {
      throw new WrongAgentStateException("Wrong state to continue movement");
    }
  }

  private boolean haveSpaceToMove(final InDirectedMovement s) {
    return s.distanceFromStart() + s.speed() < s.road().roadLength();
  }

  private InDirectedMovement moveAgent(final InDirectedMovement s) {
    return AgentState.inMovement(
        s.speed(),
        s.movingTowards(),
        s.road(),
        s.distanceFromStart() + s.speed());
  }

  private OnRoadCrossing finishMovement(final InDirectedMovement s) {
    return AgentState.onRoadCrossing(s.movingTowards());
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    final ContinueMovementInCurrentDirection that = (ContinueMovementInCurrentDirection) o;

    return agent.equals(that.agent);
  }

  @Override
  public int hashCode() {
    return agent.hashCode();
  }

  @Override
  public String toString() {
    return "ContinueMovementInCurrentDirection{" +
        "agent=" + agent +
        '}';
  }
}