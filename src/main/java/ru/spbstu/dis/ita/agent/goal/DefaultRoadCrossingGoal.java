package ru.spbstu.dis.ita.agent.goal;

import ru.spbstu.dis.ita.environment.RoadCrossing;

public class DefaultRoadCrossingGoal implements RoadCrossingGoal {

  private final RoadCrossing goal;

  DefaultRoadCrossingGoal(RoadCrossing goal) {
    this.goal = goal;
  }

  public RoadCrossing goal() {
    return goal;
  }

  @Override
  public RoadCrossing crossing() {
    return goal;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final DefaultRoadCrossingGoal that = (DefaultRoadCrossingGoal) o;
    return goal.equals(that.goal);
  }

  @Override
  public int hashCode() {
    return goal.hashCode();
  }
}
