package ru.spbstu.dis.ita.agent.decision;

import ru.spbstu.dis.ita.agent.Agent;
import ru.spbstu.dis.ita.agent.state.AgentState;
import ru.spbstu.dis.ita.environment.RoadCrossing;
import ru.spbstu.dis.ita.environment.TransportEnvironment;

public interface AgentDecision {
  /**
   * Returns an agent who made a decision for himself
   * @return
   */
  Agent decisionOriginator();

  /**
   * Implements decision. Decision implementation execution time = 1 epoch
   */
  void implementDecision(TransportEnvironment environment);

  static ContinueMovementInCurrentDirection continueMovementInCurrentDirection(Agent agent) {
    return new ContinueMovementInCurrentDirection(agent);
  }

  static StartMovementInDirection startMovementInDirection(RoadCrossing towards, Agent agent) {
    return new StartMovementInDirection(towards, agent);
  }

  static StayOnRoadCrossing stayOnRoadCrossing(RoadCrossing crossing, Agent agent) {
    return new StayOnRoadCrossing(crossing, agent);
  }
}

