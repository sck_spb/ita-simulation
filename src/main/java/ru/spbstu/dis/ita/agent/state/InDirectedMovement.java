package ru.spbstu.dis.ita.agent.state;

import ru.spbstu.dis.ita.environment.Road;
import ru.spbstu.dis.ita.environment.RoadCrossing;

public interface InDirectedMovement extends AgentState {
  /**
   * Movement speed
   * @return
   */
  double speed();

  /**
   * Distance from the crossing where the movement started from
   * @return
   */
  double distanceFromStart();

  Road road();

  RoadCrossing movingTowards();
}
