package ru.spbstu.dis.ita.agent.knowledgebase;

import com.fuzzylite.Engine;
import com.fuzzylite.rule.Rule;
import com.fuzzylite.rule.RuleBlock;
import com.fuzzylite.variable.InputVariable;
import com.fuzzylite.variable.OutputVariable;
import ru.spbstu.dis.ita.agent.PerceivedData;

import java.util.*;
import java.util.stream.Collectors;

public class LocalKnowledgeBase implements KnowledgeBase {
	private List<RuleBlock> ruleBlocks = new LinkedList<>();
	private Map<InputVariable, List<String>> inputVariableRules = new HashMap<>();
	private List<KnowledgeBaseRule> knowledgeBaseRules = new LinkedList<>();
	private List<OutputVariable> outputVariables = new LinkedList<>();
	private Engine engine = new Engine();

	public void updateFuzzyRules() {
		outputVariables.forEach(outputVariable -> engine.addOutputVariable(outputVariable));
		inputVariableRules.forEach((input, rules) -> {
			engine.addInputVariable(input);
			RuleBlock ruleblock = new RuleBlock();
			rules.forEach(rule -> ruleblock.addRule(Rule.parse(rule, engine)));
			ruleBlocks.add(ruleblock);
		});
	}

	@Override
	public List<KnowledgeBaseMultiplierCoefficient> computeKBMultiplierCoefficients() {
		engine.setName("ITA KnowledgeBase Engine");
		updateFuzzyRules();
		ruleBlocks.forEach(ruleBlock -> engine.addRuleBlock(ruleBlock));
		// todo move configuration to field
		engine.configure("AlgebraicProduct", "", "Minimum", "Maximum", "Centroid");
		// call process
		engine.process();
		// get results
		List<KnowledgeBaseMultiplierCoefficient> outputCoefficiens = new LinkedList<>();
		outputVariables.forEach(outputVariable -> outputCoefficiens.add(
				new KnowledgeBaseMultiplierCoefficient(outputVariable.getName(), outputVariable.getOutputValue())));
		return outputCoefficiens;
	}

	@Override
	public void setRuleData(List<PerceivedData> inputValues,
			List<KnowledgeBaseMultiplierCoefficient> multiplierCoefficients,
			List<KnowledgeBaseRule> knowledgeBaseRules) {
		this.knowledgeBaseRules.addAll(knowledgeBaseRules);
		knowledgeBaseRules.stream()
				.map(rule -> new KnowledgeBaseFuzyRule(rule.getRuleAsString(), engine))
				.collect(Collectors.toList());
		inputVariableRules.putAll(KnowledgeBaseDataAdapter.generateFuzzyInput(inputValues, knowledgeBaseRules.stream()
				.map(knowledgeBaseRule -> knowledgeBaseRule.getRuleAsString()).collect(Collectors.toList())));
		outputVariables.addAll(KnowledgeBaseDataAdapter.generateFuzzyOutput(multiplierCoefficients));
	}
}
