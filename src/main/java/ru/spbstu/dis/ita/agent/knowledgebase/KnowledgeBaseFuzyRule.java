package ru.spbstu.dis.ita.agent.knowledgebase;

import com.fuzzylite.Engine;
import com.fuzzylite.rule.Rule;

public class KnowledgeBaseFuzyRule implements KnowledgeBaseRule {
	private Engine engine;
	private String rule;

	public KnowledgeBaseFuzyRule(String fuzzyRule, Engine engine) {
		this.engine = engine;
	}

	public KnowledgeBaseFuzyRule(String rule) {
		this.rule = rule;
	}

	@Override
	public String getRuleAsString() {
		return rule;
	}
	@Override
	public Object getRuleObject() {
		return Rule.parse(rule, engine);
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public String getRule() {
		return rule;
	}
}
