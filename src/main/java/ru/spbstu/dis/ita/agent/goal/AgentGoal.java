package ru.spbstu.dis.ita.agent.goal;

import ru.spbstu.dis.ita.environment.RoadCrossing;

public interface AgentGoal {

  static RoadCrossingGoal reachRoadCrossing(RoadCrossing crossingToReach) {
    return new DefaultRoadCrossingGoal(crossingToReach);
  }
}
