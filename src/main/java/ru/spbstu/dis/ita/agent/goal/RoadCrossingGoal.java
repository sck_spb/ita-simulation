package ru.spbstu.dis.ita.agent.goal;

import ru.spbstu.dis.ita.environment.RoadCrossing;

public interface RoadCrossingGoal extends AgentGoal {
  RoadCrossing crossing();
}
