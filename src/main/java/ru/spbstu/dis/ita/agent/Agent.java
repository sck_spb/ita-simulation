package ru.spbstu.dis.ita.agent;

import ru.spbstu.dis.ita.agent.decision.AgentDecision;
import ru.spbstu.dis.ita.agent.goal.AgentGoal;
import ru.spbstu.dis.ita.agent.state.AgentState;
import ru.spbstu.dis.ita.environment.TransportEnvironment;
import ru.spbstu.dis.ita.messaging.Message;
import java.util.List;

public interface Agent {
  default void executeLifeCycle(TransportEnvironment environment) {
    handlePerceivedData(perceiveData(environment));
    handleIncomingMessages(environment.incomingMessagesFor(this));
    environment.registerOutgoingMessages(this, generateOutgoingMessages());
    environment.registerDecision(solveAndDecide(environment));
  }

  /**
   * Agent lifecycle function
   * @param environment
   * @return
   */
  PerceivedData perceiveData(TransportEnvironment environment);

  /**
   * Agent lifecycle function
   * @param newData
   */
  void handlePerceivedData(PerceivedData newData);

  /**
   * @param incomingMessagesBuffer
   */
  void handleIncomingMessages(List<Message> incomingMessagesBuffer);

  /**
   * Agent lifecycle function
   * @return
   */
  List<Message> generateOutgoingMessages();

  /**
   * Agent lifecycle function
   * @param environment
   * @return
   */
  AgentDecision solveAndDecide(final TransportEnvironment environment);

  /**
   * Queriable internal state of agent
   * @return
   */
  AgentState agentState();

  void setNewAgentState(AgentState newAgentState);

  /**
   * Current final goal of agent
   * @return
   */
  AgentGoal agentGoal();
}