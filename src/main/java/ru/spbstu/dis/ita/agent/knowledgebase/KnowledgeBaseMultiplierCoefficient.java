package ru.spbstu.dis.ita.agent.knowledgebase;

public class KnowledgeBaseMultiplierCoefficient {
	String name;
	Double coefficient;

	public KnowledgeBaseMultiplierCoefficient(String name) {
		this.name = name;
	}

	public KnowledgeBaseMultiplierCoefficient(String name, Double coefficient) {
		this.name = name;
		this.coefficient = coefficient;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(Double coefficient) {
		this.coefficient = coefficient;
	}
}
