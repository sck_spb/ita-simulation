package ru.spbstu.dis.ita.agent;

import ru.spbstu.dis.ita.agent.decision.AgentDecision;
import ru.spbstu.dis.ita.agent.goal.AgentGoal;
import ru.spbstu.dis.ita.agent.state.AgentState;
import ru.spbstu.dis.ita.environment.TransportEnvironment;
import ru.spbstu.dis.ita.messaging.Message;

import java.util.List;

public class CommunicatingAgent implements Agent {
	@Override public PerceivedData perceiveData(TransportEnvironment environment) {
		return null;
	}

	@Override public void handlePerceivedData(PerceivedData newData) {

	}

	@Override public void handleIncomingMessages(List<Message> incomingMessagesBuffer) {

	}

	@Override public List<Message> generateOutgoingMessages() {
		return null;
	}

	@Override public AgentDecision solveAndDecide(TransportEnvironment environment) {
		return null;
	}

	@Override public AgentState agentState() {
		return null;
	}

	@Override public void setNewAgentState(AgentState newAgentState) {

	}

	@Override public AgentGoal agentGoal() {
		return null;
	}
}
