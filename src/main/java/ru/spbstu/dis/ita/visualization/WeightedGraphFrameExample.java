package ru.spbstu.dis.ita.visualization;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.jgrapht.ext.JGraphXAdapter;
import org.jgrapht.graph.DefaultWeightedEdge;
import com.mxgraph.layout.mxCircleLayout;
import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.swing.mxGraphComponent;
import org.jgrapht.graph.SimpleWeightedGraph;
import ru.spbstu.dis.ita.environment.Road;
import ru.spbstu.dis.ita.environment.RoadCrossing;
import ru.spbstu.dis.ita.environment.RoadNetwork;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class WeightedGraphFrameExample {
	private static void createAndShowGui(SimpleWeightedGraph g) {
		JFrame frame = new JFrame("DemoGraph");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JGraphXAdapter<RoadCrossing, Road> graphAdapter = new JGraphXAdapter<>(g);

		mxIGraphLayout layout = new mxCircleLayout(graphAdapter);
		layout.execute(graphAdapter.getDefaultParent());

		mxGraphComponent mxGraphComponent = new mxGraphComponent(graphAdapter);
		frame.add(mxGraphComponent);

		frame.pack();
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
		Random rnd = new Random();
		ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
		scheduler.scheduleAtFixedRate(() -> {
			Object[] vertexes = g.vertexSet().toArray();
			int i = rnd.nextInt(vertexes.length);
			int nextInt = rnd.nextInt(vertexes.length);
			while (i == nextInt) {
				nextInt = rnd.nextInt(vertexes.length);
			}
			Object edge = g.getEdge(vertexes[nextInt], vertexes[i]);

			if (edge != null) {
				System.out.println(vertexes[i] + ";" + vertexes[nextInt]);
				g.setEdgeWeight(edge, rnd.nextInt(100));
			}
			mxGraphComponent.refresh();
			frame.revalidate();
			frame.repaint();

		}, 0, 400, TimeUnit.MILLISECONDS);
	}

	public static void main(String[] args) {

		SimpleWeightedGraph g =  buildGraph();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGui(g);
			}
		});
	}

	public static class MyEdge extends DefaultWeightedEdge {
		@Override public String toString() {
			return String.valueOf(getWeight());
		}

		@Override public boolean equals(Object o) {
			if (this == o)
				return true;
			if (!(o instanceof MyEdge))
				return false;

			MyEdge myEdge = (MyEdge) o;

			if (getSource() != null ? !getSource().equals(myEdge.getSource()) : myEdge.getSource() != null)
				return false;
			return !(getTarget() != null ? !getTarget().equals(myEdge.getTarget()) : myEdge.getTarget() != null);

		}

		@Override public int hashCode() {
			int result = getSource() != null ? getSource().hashCode() : 0;
			result = 31 * result + (getTarget() != null ? getTarget().hashCode() : 0);
			return result;
		}
	}

	public static SimpleWeightedGraph<RoadCrossing, Road> buildGraph() {
		final RoadNetwork roadNetwork = new RoadNetwork();
		roadNetwork.addRoadCrossing(new RoadCrossing(0, 1));
		roadNetwork.addRoadCrossing(new RoadCrossing(1, 0));
		roadNetwork.addRoadCrossing(new RoadCrossing(0, 0));
		roadNetwork.addRoadCrossing(new RoadCrossing(1, 1));
		roadNetwork.addRoadCrossing(new RoadCrossing(2, 1));
		roadNetwork.addRoadCrossing(new RoadCrossing(1,2));
		roadNetwork.addRoadCrossing(new RoadCrossing(2, 0));
		roadNetwork.addRoadCrossing(new RoadCrossing(0, 2));
		roadNetwork.addRoadCrossing(new RoadCrossing(2, 2));

		// when
		roadNetwork.addRoad(new Road(new RoadCrossing(0, 1), new RoadCrossing(1, 0)));
		roadNetwork.addRoad(new Road(new RoadCrossing(0, 1), new RoadCrossing(1, 1)));
		roadNetwork.addRoad(new Road(new RoadCrossing(1, 1), new RoadCrossing(1, 2)));
		roadNetwork.addRoad(new Road(new RoadCrossing(2, 1), new RoadCrossing(0, 0)));
		roadNetwork.addRoad(new Road(new RoadCrossing(2, 1), new RoadCrossing(0, 0)));
		roadNetwork.addRoad(new Road(new RoadCrossing(2, 2), new RoadCrossing(1, 1)));
		roadNetwork.addRoad(new Road(new RoadCrossing(2, 0), new RoadCrossing(0, 2)));
		roadNetwork.addRoad(new Road(new RoadCrossing(2, 0), new RoadCrossing(0,1)));

		return roadNetwork.graph();
	}
}